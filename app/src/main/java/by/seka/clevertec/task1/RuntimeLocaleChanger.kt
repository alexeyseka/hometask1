package by.seka.clevertec.task1

import android.content.Context
import android.content.res.Configuration
import java.util.*

object RuntimeLocaleChanger {

    fun wrapContext(context: Context): Context {

        val preferredLanguage = LanguagePreferences(context).getLocale()

        Locale.setDefault(Locale(preferredLanguage))

        val newConfig = Configuration()
        newConfig.setLocale(Locale(preferredLanguage))

        return context.createConfigurationContext(newConfig)
    }

}