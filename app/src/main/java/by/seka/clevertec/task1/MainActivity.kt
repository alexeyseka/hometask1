package by.seka.clevertec.task1

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.appcompat.app.AppCompatActivity
import by.seka.clevertec.task1.databinding.ActivityMainBinding
import kotlinx.coroutines.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val scope = CoroutineScope(Dispatchers.Main)

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(RuntimeLocaleChanger.wrapContext(base))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val itVisible = LanguagePreferences(this).getHintVisibility()

        with(binding) {
            menuButton.apply {
                setOnClickListener { showPopupMenu(this) }
            }
            if (itVisible) {
                menuHint.visibility = View.VISIBLE
                startRotation()
            }
        }
    }

    private fun startRotation() {

        val flipAnimation =
            AnimatorInflater.loadAnimator(
                this,
                R.animator.rotate_animator
            ) as AnimatorSet

        scope.launch {
            while (true) {
                flipAnimation.setTarget(binding.menuHint)
                flipAnimation.start()
                delay(2000)
            }
        }
    }

    private fun showPopupMenu(view: View) {

        val menu = PopupMenu(this, view)
        menu.inflate(R.menu.language_menu)
        menu.show()

        menu.setOnMenuItemClickListener {

            setNewLanguage(it.toString())
            menu.dismiss()
            LanguagePreferences(this).setHintInvisible()
            binding.menuHint.visibility = View.GONE
            scope.cancel()
            recreate()
            true
        }
    }

    private fun setNewLanguage(chosenItem: String) {
        val chosenLocale = when (chosenItem) {
            "中國人" -> "zh"
            "Shqiptare" -> "sq"
            "Français" -> "fr"
            "Deutsch" -> "de"
            "Magyar" -> "hu"
            "Português" -> "pt"
            "Русский" -> "ru"
            "ไทย" -> "th"
            else -> Locale.getDefault().language

        }
        LanguagePreferences(this).setLocale(chosenLocale)
    }
}

