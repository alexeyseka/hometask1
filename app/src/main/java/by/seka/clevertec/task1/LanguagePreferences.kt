package by.seka.clevertec.task1

import android.content.Context
import java.util.*

class LanguagePreferences(private val context: Context) {

    fun getLocale(): String {
        return context.getSharedPreferences(LOCALE, Context.MODE_PRIVATE)
            .getString(LOCALE, Locale.getDefault().language).toString()
    }

    fun setLocale(tag: String) {
        context.getSharedPreferences(LOCALE, Context.MODE_PRIVATE).edit().putString(LOCALE, tag)
            .apply()
    }

    fun getHintVisibility(): Boolean {
        return context.getSharedPreferences("visibility", Context.MODE_PRIVATE)
            .getBoolean("visibility", true)

    }

    fun setHintInvisible() {
        context.getSharedPreferences("visibility", Context.MODE_PRIVATE).edit()
            .putBoolean("visibility", false)
            .apply()
    }

}