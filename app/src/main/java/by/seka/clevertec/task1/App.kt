package by.seka.clevertec.task1

import android.app.Application
import android.content.Context

class App : Application() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(RuntimeLocaleChanger.wrapContext(base))
    }
}